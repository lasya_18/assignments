=>Task #1

function isEquals(x, y) {
  return x == y;
}
console.log(isEquals(3,3));

=>Task #2
function isBigger(x, y) {
  return x > y;
}
console.log(isBigger(5,-1));

=>Task #4
function getDifference(x, y) {
  return x - y;
}
console.log(getDifference(5,-1));

=>Task #5
function negativeCount(...args) {
    let c=0;
  for(let i of args){
      if(i<0)
        c+=1;
  }
  return c;  
}
console.log(negativeCount(4, -3, 2, -9));

=>Task #6
function letterCount(x,y) {
    let c=0;
  for(let i of x){
      if(i==y)
        c+=1;
  }
  return c;  
}
console.log(letterCount("Barny", "y"));

=>Task #7
